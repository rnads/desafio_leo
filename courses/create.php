<?php
include('../controllers/courses.php');
$courses = new courses();
$courses->store();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DESAFIO LEO</title>
</head>

<link rel="stylesheet" href="../public/css/bootstrap.min.css">

<body>

    <div class="container mt-5">
        <div class="card">
            <div class="card-header">
                CRIAR CURSOS
            </div>
            <div class="card-body">
                <form action="./create.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="method" value="create">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <label for="">Imagem</label>
                            <div class="input-group mb-3">
                                <input type="file" name="src" class="form-control" id="inputGroupFile01" required>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <label for="">Título</label>
                            <input type="text" name="title" class="form-control" placeholder="Título" required>
                        </div>

                        <div class="col-12 col-md-4">
                            <label for="">Link</label>
                            <input type="text" name="url" class="form-control" placeholder="Link de redirecionamento" required>
                        </div>
                    </div>

                    <div class="row justify-content-md-center">
                        <div class="col-8">
                            <label for="">Descrição</label>
                            <textarea name="description" class="form-control" cols="10" rows="5"></textarea>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <div class="col-2">
                            <a href="../courses/" class="btn btn-danger w-100">VOLTAR</a>
                        </div>
                        <div class="col-2">
                            <button type="submit" class="btn btn-success w-100">Salvar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

    <script src="https://kit.fontawesome.com/638c2e6110.js" crossorigin="anonymous"></script>
    <script src="../public/js/bootstrap.bundle.min.js"></script>
</body>

</html>