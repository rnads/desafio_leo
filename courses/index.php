<?php
include('../controllers/courses.php');
$courses = new courses();
$courses->delete();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DESAFIO LEO</title>
</head>

<link rel="stylesheet" href="../public/css/bootstrap.min.css">

<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <a href="../" class="btn btn-primary">VOLTAR</a>
                ADMINISTRAR CURSOS
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Image</th>
                            <th scope="col">Title</th>
                            <th scope="col">Url</th>
                            <th scope="col">
                                <a href="./create.php" class="btn btn-primary btn-sm">Novo</a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($courses->index() as $course) {
                        ?>
                            <tr>
                                <th scope="row"><?= $course['id'] ?></th>
                                <td>
                                    <img src="../public/images/courses/<?= $course['src'] ?>" alt="<?= $course['title'] ?>">
                                </td>
                                <td><?= $course['title'] ?></td>
                                <td><?= $course['url'] ?></td>
                                <td>
                                <form action="./edit.php" method="POST">
                                        <input type="hidden" name="method" value="edit">
                                        <input type="hidden" name="id" value="<?= $course['id'] ?>">
                                        <button type="submit" class="btn btn-warning btn-sm">Editar</button>
                                    </form>
                                    <form action="./index.php" method="POST">
                                        <input type="hidden" name="method" value="delete">
                                        <input type="hidden" name="id" value="<?= $course['id'] ?>">
                                        <button type="submit" class="btn btn-danger btn-sm">Excluir</button>
                                    </form>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="https://kit.fontawesome.com/638c2e6110.js" crossorigin="anonymous"></script>
    <script src="../public/js/bootstrap.bundle.min.js"></script>
</body>

</html>