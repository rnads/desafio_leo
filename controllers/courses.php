<?php

class courses
{
    public $con;

    function __construct()
    {
        $this->con = new mysqli('127.0.0.1', 'root', '', 'desafio_leo');

        if ($this->con->connect_error) {
            die("Connection failed: " . $this->con->connect_error);
        }
    }


    public function index()
    {
        $sql = "SELECT * FROM courses";
        $courses = $this->con->query($sql);

        return $courses;
    }

    public function store()
    {
        $this->con->begin_transaction();
        try {
            if (isset($_POST['method'])) {
                if ($_POST['method'] == 'create') {
                    $file = basename($_FILES['src']['name']);

                    $name = uniqid('') . '.' . pathinfo($file, PATHINFO_EXTENSION);

                    $dir = str_replace('courses', 'public/images/courses/', getcwd()) . $name;
                    move_uploaded_file($_FILES['src']['tmp_name'], $dir);

                    $title = $_POST['title'];
                    $description = $_POST['description'];
                    $url = $_POST['url'];

                    $sql = "INSERT INTO courses VALUES (NULL, '$title', '$name', '$description', '$url')";

                    if ($this->con->query($sql) === TRUE) {
                        echo "New record created successfully";
                    }
                }
            }
            $this->con->commit();
        } catch (mysqli_sql_exception $exception) {
            $this->con->rollback();

            throw $exception;
        }
    }


    public function edit()
    {
        if (isset($_POST['method'])) {
            if ($_POST['method'] == 'edit') {
                $id = $_POST['id'];

                $sql = "SELECT * FROM courses WHERE id = $id";

               $course = $this->con->query($sql);

               return mysqli_fetch_assoc($course);
            }
        }
    }

    public function update()
    {
        $this->con->begin_transaction();
        try {
            if (isset($_POST['method'])) {
                if ($_POST['method'] == 'update') {
                    $file = basename($_FILES['src']['name']);

                    $name = uniqid('') . '.' . pathinfo($file, PATHINFO_EXTENSION);

                    $dir = str_replace('courses', 'public/images/courses/', getcwd()) . $name;
                    move_uploaded_file($_FILES['src']['tmp_name'], $dir);

                    $title = $_POST['title'];
                    $description = $_POST['description'];
                    $url = $_POST['url'];
                    $id = $_POST['id'];

                    $sql = "UPDATE courses SET title = '$title', description = '$description', url = '$url', src = '$name' WHERE id = $id";

                    if ($this->con->query($sql) === TRUE) {
                        header("Location: ./");
                    }
                }
            }
            $this->con->commit();
        } catch (mysqli_sql_exception $exception) {
            $this->con->rollback();

            throw $exception;
        }
    }


    public function delete()
    {
        $this->con->begin_transaction();
        try {
            if (isset($_POST['method'])) {
                if ($_POST['method'] == 'delete') {
                    $id = $_POST['id'];

                    $sql = "DELETE FROM courses WHERE id = $id";

                    if ($this->con->query($sql) === TRUE) {
                        echo "Record deleted successfully";
                    } else {
                        echo "Error: " . $sql . "<br>" . $this->con->error;
                    }
                }
            }
            $this->con->commit();
        } catch (mysqli_sql_exception $exception) {
            $this->con->rollback();

            throw $exception;
        }
    }
}
