<?php
include('./controllers/courses.php');
$courses = new courses();

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DESAFIO LEO</title>
</head>

<link rel="stylesheet" href="./public/css/bootstrap.min.css">
<link rel="stylesheet" href="./public/css/custom.css">

<body>

    <?php
    include('./layouts/header.php');
    include('./layouts/slide.php');
    include('./layouts/courses.php');
    include('./layouts/footer.php');
    include('./layouts/modal.php');
    ?>
    <script src="https://kit.fontawesome.com/638c2e6110.js" crossorigin="anonymous"></script>
    <script src="./public/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="./public/js/coockie.js"></script>  
</body>

</html>