<section>
    <div class="container mt-5 border-">
        <h3 class="text-secondary">Meus Cursos</h3>
        <div class="col-12 border-bottom"></div>

        <div class="row mt-5">
            <?php
            foreach ($courses->index() as $course) {
            ?>
                <div class="col-12 col-md-3 m-1">
                    <div class="card">
                        <img src="./public/images/courses/<?= $course['src'] ?>" class="card-img-top" alt="CURSO">
                        <div class="card-body">
                            <h5 class="card-title"><?= $course['title'] ?></h5>
                            <p class="card-text  text-truncate"><?= $course['description'] ?></p>
                            <a href="<?= $course['url'] ?>" target="_blank" class="btn btn-success custom-button w-100">Ver curso</a>
                        </div>
                    </div>
                </div>
            <?php
            }
            ?>

            <div class="col-12 col-md-3 m-1">
                <a href="<?=$_SERVER['REQUEST_URI']?>courses">
                    <div class="card card-custom text-center">
                        <div class="card-body mt-5">
                            <i class="far fa-file fa-9x"></i>
                            <h5 class="card-title">ADICIONAR CURSO</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>