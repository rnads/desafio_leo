<section>
        <nav class="navbar navbar-white bg-white">
            <div class="container-fluid d-flex align-middle">

                <div class="flex-grow-1">
                    <a class="navbar-brand " href="#">
                        <img src="./public/images/logo.png" alt="log-menu" class="d-inline-block align-text-top logo_menu">
                    </a>
                </div>

                <div class="p-2  d-none d-sm-block">
                    <div class="submit-line">
                        <input type="text" class="form-control custom-button" placeholder="Pesquisar cursos..." />
                        <button class="submit-lente" type="submit">
                            <i class="fa fa-search"></i>
                        </button>
                    </div>

                </div>

                <div class="divider d-none d-sm-block"></div>

                <div class="p-2 ">
                    <a class="navbar-brand " href="#">
                        <img src="./public/images/user.png" alt="" width="40" height="40" class="d-inline-block align-text-top">
                    </a>
                </div>

                <div class="p-2 text-right">
                    Seja bem-vindo
                    <p>John Doe</p>
                </div>

                <!-- <div class="p-2">
            </div> -->
            </div>
        </nav>
    </section>

