<section class="footer">
    <div class="row">
        <div class="col-12 col-md-6 p-2 logo_text">
            <img src="./public/images/logo.png" alt="logo_footer">
            <p class="text-secondary">
                Some quick example text to build on the card title and make up
                <br> the bulk of the card's content.
            </p>
        </div>
        <div class="col-12 col-md-3 contact_text text-center">
            <h4>// CONTATO </h4>
            <h5 class="text-secondary">(21) 98765-3434</h5>
            <h5 class="text-secondary">contato@leolearning.com</h5>
        </div>
        <div class="col-12 col-md-3 socials_text text-center">
            <h4>// REDES SOCIAIS </h4>
            <a href="#"><i class="fab fa-twitter fa-3x text-secondary"></i></a>
            <a href="#"><i class="fab fa-youtube fa-3x text-secondary"></i></a>
            <a href="#"><i class="fab fa-pinterest-p fa-3x text-secondary"></i></a>
        </div>
    </div>
</section>

<section>
    <p class="p-2 text-secondary">Copyright 2017 - All right reserved.</p>
</section>